from django.urls import include, path
from rest_framework import routers
from .views import InsurerView, InsurerResponsesView
from django.views.decorators.csrf import csrf_exempt

app_name = "risks"

# router = routers.DefaultRouter()
# router.register("", InsurerView)
# router.register("responses", InsurerResponsesView)

urlpatterns = [
    path("", InsurerView.as_view({"get": "list", "post": "create"}), name="risks"),
    path("responses/", csrf_exempt(InsurerResponsesView.as_view()), name="responses"),
]