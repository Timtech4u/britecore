from rest_framework import serializers
from .models import Insurer, InsurerResponses

class InsurerResponsesSerializer(serializers.ModelSerializer):
    """
        Model Serializer for Model InsurerResponses
    """
    
    class Meta:
        model = InsurerResponses
        fields = "__all__"

class InsurerSerializer(serializers.ModelSerializer):
    """
        Model Serializer for Model Insurer
    """
    responses = InsurerResponsesSerializer(many=True, required=False)

    class Meta:
        model = Insurer
        fields = "__all__"
        depth = 1

