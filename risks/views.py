from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import generics
from .models import Insurer, InsurerResponses
from .serializers import InsurerSerializer, InsurerResponsesSerializer

class InsurerView(viewsets.ModelViewSet):
    """
        Handles API Crud Actions for Risk Insurer
    """
    queryset = Insurer.objects.all()
    serializer_class = InsurerSerializer


class InsurerResponsesView(generics.CreateAPIView):
    """
        Handles API for creating Insurer Responses
    """
    serializer_class = InsurerResponsesSerializer