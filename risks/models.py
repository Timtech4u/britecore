from django.db import models
from django.contrib.postgres.fields import JSONField

class Insurer(models.Model):
    """
        Model to store Risk-Insurer information
        `name` represents Insurer name
        `fields` represents Insurer fields for form
    """
    name = models.CharField(max_length=200)
    fields = JSONField()

    def __str__(self):
        return self.name


class InsurerResponses(models.Model):
    """
        Model to store Risk-Insurer Form Responses
        `date` for date submitted
        `response` for single form submission
        `insurer` for insurer that owns responses
    """
    date = models.DateTimeField(auto_now_add=True, editable=False)
    response = JSONField()
    insurer = models.ForeignKey("Insurer", related_name="responses", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Insurer Response"
        verbose_name_plural = "Insurer Responses"
    
    def __str__(self):
        return str(self.date)