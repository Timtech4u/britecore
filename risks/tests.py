from django.test import TestCase
from rest_framework.test import APIClient

class TestInsurer(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.uri = "/risks/"
    
    def test_list(self):
        """
        Ensure we can get a list of insurer forms from API endpoint
        """
        response = self.client.get(self.uri)
        self.assertEqual(response.status_code, 200, "Expected to get Response Code of 200, but received {}".format(response.status_code))
    
    def test_create(self):
        """
        Ensure we can create an insurer form from API endpoint
        """
        params = {
            "name": "FakeHospitalInsurer",
            "fields": [{
                "name": "Patient Name",
                "type": "input",
                "label": "Patient Name",
                "inputType": "text",
                "placeholder": "Please enter your patient name"
            },
            {
                "name": "Patient Age",
                "type": "input",
                "label": "Patient Age",
                "model": "patient age",
                "inputType": "number",
                "placeholder": "Please enter your patient age"
            },
            {
                "name": "Patient Gender",
                "type": "select",
                "label": "Patient Gender",
                "model": "patient gender",
                "options": [
                    "Male",
                    "Female",
                ],
                "placeholder": "Please select your patient gender"
            }]
        }
        response = self.client.post(self.uri, params, format="json")
        self.assertEqual(response.status_code, 201, "Expected to get Response Code of 201, but received {}".format(response.status_code))
