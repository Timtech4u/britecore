from django.contrib import admin
from .models import Insurer, InsurerResponses

admin.site.register(Insurer)
admin.site.register(InsurerResponses)