# BriteCore Software Engineer (Product Development) Challenge

## Basic User Flow
- Visit or Click on *Build Insurer Form*  [here](http://tim-britecore.s3-website-us-east-1.amazonaws.com/#/build)
- Visit or Click on *Insurer Forms* and access all created Insurer Forms.
- To make response: Click on *View Form* 
- To view responses: Click on *View Responses*
- You can also Download responses

## Deliverables

### Data

* Python file containing ORM: [here](./migrations.py)
* Entity Relationship Diagram: [here](./BriteCoreAPIModel.jpg)

### Backend

* REST API Documentation: [here](https://tim-britecore.tk/docs)
* Django REST Framework Class Based Views: [here](./risks/views.py)

### Frontend
- Deployed Version: [here](http://tim-britecore.s3-website-us-east-1.amazonaws.com)
- *Screenshots* [here](#screenshots)

### Final Deliverables
- Backend Codes on GitLab: [here](https://gitlab.com/Timtech4u/britecore)
- Frontend Codes on GitLab: [here](https://gitlab.com/Timtech4u/britecore-ui)
- Codes solution to test: [here](./quiz.py)

## Technologies Used

### Backend
- Django
- Django Rest Framework
- Docker & Docker Compose
- AWS EC2
- GitLab CI

### Frontend
- Vue.js
- Vuex
- Axios
- Element UI
- AWS S3

## [Tests](./risks/tests.py)
- To Test Risk Insurer Create: `python manage.py test risks.tests.TestInsurerResponses.test_create`
- To Test Risk Insurer List: `python manage.py test risks.tests.TestInsurerResponses.test_list`

## Screenshots

### Building an Insurer Form
![build](ui/build.PNG)

### Listing all Insurer Forms
![list](ui/list.PNG)

### An Insurer Form
![form](ui/form.PNG)

### An Insurer Table
![table](ui/table.PNG)

### Downloaded Response
![excel](ui/excel.PNG)

### Project Setup and Deployment Docs: [here](./SETUP.rst)