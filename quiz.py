from cryptography.fernet import Fernet

key = 'TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM='

# Oh no! The code is going over the edge! What are you going to do?
message = b'gAAAAABdSphuiSiGreATtfDhKiImb8EXNhYYk3IDG4fhhJcjhs5lLcMMKWPX2vffBX-1gztvWlc8gK7In2VGJ_GeAK_HvdiVDg6T9H28Pz7tpYCGlkUDf85YUfd2ALadOVOkAajumhBUkiV1QRikBc-vNzlaDeE5jSlRUeo30dOMHAdGKQyFMbA='

def main():
    f = Fernet(key)
    print(f.decrypt(message).decode('utf-8'))


if __name__ == "__main__":
    main()